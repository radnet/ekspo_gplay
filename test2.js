var gplay = require('google-play-scraper');


var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'adminuser',
  password : 'tragarz',
  database : 'node_google'
});

connection.connect();

 connection.query('SELECT 1 + 1 AS solution', function (error, results, fields) {
  if (error) throw error;
  console.log('MySql connected ');
}); 

var day = 1;

var cats = [];
cats.push(gplay.category.APPLICATION);
cats.push(gplay.category.BUSINESS);
cats.push(gplay.category.COMMUNICATION);
cats.push(gplay.category.DATING);
cats.push(gplay.category.EDUCATION);
cats.push(gplay.category.ENTERTAINMENT);
cats.push(gplay.category.EVENTS);
cats.push(gplay.category.FINANCE);
cats.push(gplay.category.HEALTH_AND_FITNESS);
cats.push(gplay.category.HOUSE_AND_HOME);
cats.push(gplay.category.LIFESTYLE);
cats.push(gplay.category.MAPS_AND_NAVIGATION);
cats.push(gplay.category.PHOTOGRAPHY);
cats.push(gplay.category.SHOPPING);
cats.push(gplay.category.SPORTS);
cats.push(gplay.category.TOOLS);
cats.push(gplay.category.TRAVEL_AND_LOCAL);
cats.push(gplay.category.WEATHER);
cats.push(gplay.category.GAME);
cats.push(gplay.category.FAMILY);


var coll = [];
coll.push(gplay.collection.TOP_FREE);
coll.push(gplay.collection.NEW_FREE);
coll.push(gplay.collection.TOP_PAID);
coll.push(gplay.collection.NEW_PAID);


var existsApps = [];


function getApps(cat, coll, from, count) {
	return new Promise((resolve, reject) => {
		if(from !== 0)
			from = from - 1;
		
		gplay.list({
			throttle: 1,
			category: cat,
			collection: coll,
			num: count,
			start: from,
			country: "us",
			lang: "us"	
		}).then((apps) => {
			resolve({ apps: apps, from: from, count: count, cat: cat, coll: coll});
		}).catch(reject);
	}, console.log);	
}

function setRank(args) {
	return new Promise((resolve, reject) => {
		//apps = args.apps;
		from = args.from;
		
		Object.entries(args.apps).forEach(([key, val]) => {
			val.rank = (parseInt(key) + 1 + from);
		});
		resolve(args);
	});
}



function getExistApps(args) {
	return new Promise((resolve, reject) => {
		//TODO select mixID from table related to app category
		 connection.query('SELECT mixID FROM apps', function (error, results, fields) {
		  if (error) resolve(args);
		  
		  existsApps = [];
		  Object.entries(results).forEach(([key, val]) => {
			  existsApps.push(val.mixID);
		  });
		  //console.log(results);
		  resolve(args);
		  
		}); 
	});
}

var i = 0;
function forEachApp(args) {
	return new Promise((resolve, reject) => {
		i = 0;
		iterate();
		
		function iterate() {
			if(i < args.count)
				if(args.apps[i])
					forApp(i++).then(iterate);
				else {
					i++;
					iterate();
				}
			else
				resolve();
		}

		function forApp(k) {
			return new Promise((resolve, reject) => {
				//if app not exist
				var mixID = args.cat + "_" + args.coll + "_" + day + "_" + args.apps[k].rank;
				
				if (!existsApps.includes(mixID)) {
					args.apps[k].mixID = mixID;
					args.app = args.apps[k];
					saveApp(args).then(resolve);
				} else {
					//console.log("exist " + args.apps[k].appId);
					console.log("exist " + args.cat + " " + args.coll + ": " + args.apps[k].rank + ": " + args.apps[k].appId);
					resolve();
				}
	
			});
			
		}
	
	});
}


function saveApp(args) {
	return new Promise((resolve, reject) => {
		var app  = {
				catID: args.cat,
				collection: args.coll,
				day: day,
				rank: args.app.rank,
				appID: args.app.appId,
				mixID: args.app.mixID
				
			};

			var query = connection.query('INSERT INTO apps SET ?', app, function (error, results, fields) {
				if (error) console.log(error);
				console.log("add " + args.cat + " " + args.coll + ": " + args.app.rank + ": " + args.app.appId);
				resolve();
			});

		
	});
}

let size = 100;
let repeat = 5;

async function loop() {
    for (let i = 0; i < cats.length; i++) {
		for (let j = 0; j < coll.length; j++) {
			for (let k = 0; k < repeat; k++) {
				await getApps(cats[i], coll[j], k*size + 1, size)
						.then(setRank)
						.then(getExistApps)
						.then(forEachApp)
						.catch(()=>{console.log("error");});
			}
		}
    }
};


loop();