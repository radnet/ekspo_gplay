var gplay = require('google-play-scraper');


var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'adminuser',
  password : 'tragarz',
  database : 'node_google'
});

connection.connect();

 connection.query('SELECT 1 + 1 AS solution', function (error, results, fields) {
  if (error) throw error;
  console.log('MySql connected ');
}); 

var day = 1;


var cats = [];
cats.push(gplay.category.APPLICATION);
cats.push(gplay.category.BUSINESS);
cats.push(gplay.category.COMMUNICATION);
cats.push(gplay.category.DATING);
cats.push(gplay.category.EDUCATION);
cats.push(gplay.category.ENTERTAINMENT);
cats.push(gplay.category.EVENTS);
cats.push(gplay.category.FINANCE);
cats.push(gplay.category.HEALTH_AND_FITNESS);
cats.push(gplay.category.HOUSE_AND_HOME);
cats.push(gplay.category.LIFESTYLE);
cats.push(gplay.category.MAPS_AND_NAVIGATION);
cats.push(gplay.category.PHOTOGRAPHY);
cats.push(gplay.category.SHOPPING);
cats.push(gplay.category.SPORTS);
cats.push(gplay.category.TOOLS);
cats.push(gplay.category.TRAVEL_AND_LOCAL);
cats.push(gplay.category.WEATHER);
cats.push(gplay.category.GAME);
cats.push(gplay.category.FAMILY);


var coll = [];
coll.push(gplay.collection.TOP_FREE);
coll.push(gplay.collection.NEW_FREE);
coll.push(gplay.collection.TOP_PAID);
coll.push(gplay.collection.NEW_PAID);


var existsApps = [];


// gplay.app({
			// throttle: 1,
			// appId: "net.froemling.bombsquad"
		// }).then( function (appInfo) {
			
			// console.log(appInfo);
			

		// }, () => {});





function getApps(cat, coll, from, count) {
	return new Promise((resolve, reject) => {
		if(from !== 0)
			from = from - 1;
		
		var sql = 'SELECT * FROM apps WHERE catID = "' + cat + '" AND collection = "' + coll + '"  AND day = "' + day + '" LIMIT ' + count + ' OFFSET ' + from + ' ';
		//console.log(aa);
		connection.query(sql, function (error, results, fields) {
			if(error) resolve(args);
		  
			 var apps = [];
			  Object.entries(results).forEach(([key, val]) => {
				  apps.push({ appId: val.appID, rank: val.rank });
			  });
			  
			  resolve({ apps: apps, from: from, count: count, cat: cat, coll: coll});

		  
		}); 
	  
	}, console.log);	
}




function getExistApps(args) {
	return new Promise((resolve, reject) => {
		//TODO select mixID from table related to app category
		 connection.query('SELECT mixID FROM apps_data ', function (error, results, fields) {
		  if (error) resolve(args);
		  
		  existsApps = [];
		  Object.entries(results).forEach(([key, val]) => {
			  existsApps.push(val.mixID);
		  });
		  //console.log(results);
		  resolve(args);
		  
		}); 
	});
}

var i = 0;
function forEachApp(args) {
	return new Promise((resolve, reject) => {
		i = 0;
		iterate();
		
		function iterate() {
			if(i < args.count)
				if(args.apps[i])
					forApp(i++).then(iterate);
				else {
					i++;
					iterate();
				}
			else
				resolve();
		}

		function forApp(k) {
			return new Promise((resolve, reject) => {
				//if app not exist
				var mixID = args.cat + "_" + args.coll + "_" + day + "_" + args.apps[k].appId;
				
				if (!existsApps.includes(mixID)) {
					args.apps[k].mixID = mixID;
					args.app = args.apps[k];
					saveApp(args).then(resolve);
				} else {
					//console.log("exist " + args.apps[k].appId);
					console.log("exist " + args.cat + " " + args.coll + ": " + args.apps[k].rank + ": " + args.apps[k].appId);
					resolve();
				}
	
			});
			
		}
	
	});
}


function saveApp(args) {
	return new Promise((resolve, reject) => {
		gplay.app({
			throttle: 1,
			appId: args.app.appId,
			country: "us",
			lang: "us"
		}).then( function (appInfo) {
			var app  = {
				catID: args.cat,
				collection: args.coll,
				day: day,
				rank: args.app.rank,
				appID: args.app.appId,
				category: appInfo.genreId,
				title: ch(appInfo.title.replace(/[^\x00-\x7F]/g, "")),
				installs: ch(appInfo.minInstalls),
				score: ch(appInfo.score),
				ratings: ch(appInfo.ratings),
				reviews: ch(appInfo.reviews),
				hist_1: ch(appInfo.histogram['1']),
				hist_2: ch(appInfo.histogram['2']),
				hist_3: ch(appInfo.histogram['3']),
				hist_4: ch(appInfo.histogram['4']),
				hist_5: ch(appInfo.histogram['5']),
				price: appInfo.price,
				offersIAP: appInfo.offersIAP ? 1 : 0,
				size: ch(appInfo.size),
				andVer: ch(appInfo.androidVersion),
				developer: ch(appInfo.developerId),
				contentRating: ch(appInfo.contentRating),
				contentRatingDesc: ch(appInfo.contentRatingDescription) ? appInfo.contentRatingDescription : "",
				ad: ch(appInfo.adSupported) ? 1 : 0,
				released: release(appInfo.released),
				daysAfterRel: daysAfterRel(appInfo.released),
				updated: updated(appInfo.updated),
				lastUpdate: lastUpdate(appInfo.updated),
				version: appInfo.version,
				mixID: args.app.mixID
			};

			var query = connection.query('INSERT INTO apps_data SET ?', app, function (error, results, fields) {
				if (error) console.log(error);
				console.log("add " + args.cat + " " + args.coll + ": " + args.app.rank + ": " + args.app.appId);
				resolve();
			});
			 //console.log(app);
			//console.log(appInfo);
			 //resolve();

		}).catch(() => { console.log("ERROR : : " + args.app.appId); });
	});
}

function ch(item) {
	if(item)
		return item;
	else
		"";
}


function release(item) {
	if(item)
		return new Date(item).toISOString().slice(0, 11).replace('T', '');
	else 
		return null;
}

function daysAfterRel(item) {
	if(item)
		return Math.round(Math.abs((new Date().getTime() - (new Date(item)).getTime())/(24*60*60*1000)));
	else
		return -1;
}
function updated(item) {
	if(item)
		return new Date(item).toISOString().slice(0, 19).replace('T', ' ');
	else
		return null;
}

function lastUpdate(item) {
	if(item)
		return Math.round(Math.abs((new Date().getTime() - item)/(24*60*60*1000)));
	else
		return -1;
}



let size = 100;
let repeat = 5;

async function loop() {
    for (let i = 0; i < cats.length; i++) {
		for (let j = 0; j < coll.length; j++) {
			for (let k = 0; k < repeat; k++) {
				await getApps(cats[i], coll[j], k*size + 1, size)
						.then(getExistApps)
						.then(forEachApp)
						.catch(()=>{console.log("error");});
			}
		}
    }
};


loop();



  //connection.end();
