var gplay = require('google-play-scraper');


getApp("com.sirma.mobile.bible.android");
function getApp(appId) {
	return new Promise((resolve, reject) => {
		gplay.app({
			throttle: 1,
			appId: appId,
			country: "pl",
			lang: "us"	
		}).then( function (appInfo) {
			var app  = {
				appID: ch(appId),
				category: ch(appInfo.genreId),
				title: ch(appInfo.title),
				installs: ch(appInfo.minInstalls),
				score: ch(appInfo.score),
				ratings: ch(appInfo.ratings),
				reviews: ch(appInfo.reviews),
				hist_1: ch(appInfo.histogram['1']),
				hist_2: ch(appInfo.histogram['2']),
				hist_3: ch(appInfo.histogram['3']),
				hist_4: ch(appInfo.histogram['4']),
				hist_5: ch(appInfo.histogram['5']),
				price: appInfo.price,
				offersIAP: appInfo.offersIAP ? 1 : 0,
				size: ch(appInfo.size),
				andVer: ch(appInfo.androidVersion),
				developer: ch(appInfo.developerId),
				contentRating: ch(appInfo.contentRating),
				contentRatingDesc: ch(appInfo.contentRatingDescription) ? appInfo.contentRatingDescription : "",
				ad: ch(appInfo.adSupported) ? 1 : 0,
				released: release(appInfo.released),
				daysAfterRel: daysAfterRel(appInfo.released),
				updated: updated(appInfo.updated),
				lastUpdate: lastUpdate(appInfo.updated),
				version: appInfo.version
			};

			
			console.log(app);
			//console.log(appInfo);
			//resolve();

		}, () => {resolve()});
	});
}

var k = 0;

function logg(name) {
	k++;
	console.log(name);
	console.log(k);
}

function ch(item) {
	logg("ch");
	if(item)
		return item;
	else
		"";
}


function release(item) {
	logg("release");
	if(item)
		return new Date(item).toISOString().slice(0, 11).replace('T', '');
	else 
		return "";
}

function daysAfterRel(item) {
	logg("daysAfterRel");
	if(item)
		return Math.round(Math.abs((new Date().getTime() - (new Date(item)).getTime())/(24*60*60*1000)));
	else
		return "";
}
function updated(item) {
	logg("updated");
	if(item)
		return new Date(item).toISOString().slice(0, 19).replace('T', ' ');
	else
		return "";
}

function lastUpdate(item) {
	logg("lastUpdate");
	if(item)
		return Math.round(Math.abs((new Date().getTime() - item)/(24*60*60*1000)));
	else
		return "";
}
